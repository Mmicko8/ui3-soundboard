import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField} from "@mui/material";
import {Controller, useForm} from "react-hook-form";
import {Item, ItemData} from "./model/item";

interface AddItemProps {
    open: boolean;
    closeDialog: () => void;
    onSubmit: (data: ItemData) => void;
}

function AddItemDialog({open, closeDialog, onSubmit}: AddItemProps) {
    const {control, handleSubmit, reset, formState: {errors}} = useForm({
        defaultValues: {
            name: "",
            quote: "",
            image: "",
            sound: ""
        }
    });

    const handleClose = () => {
        closeDialog();
    };

    const _onSubmit = (data: ItemData) => {
        onSubmit(data);
        reset();
        handleClose();
    }

    return <Dialog open={open} onClose={handleClose}>
        <form onSubmit={handleSubmit(_onSubmit)}>
        <DialogTitle>Add Item</DialogTitle>
        <DialogContent>

                <Controller name={"name"} control={control} render={({ field }) => <TextField
                    autoFocus
                    margin="dense"
                    id="name"
                    label="Name"
                    type="text"
                    fullWidth
                    variant="standard"
                    {...field}
                /> }/>

                <Controller name={"quote"} control={control} render={({ field }) => <TextField
                    id="quote"
                    label="Quote"
                    type="text"
                    fullWidth
                    variant="standard"
                    {...field}/> }/>

                <Controller name={"image"} control={control} render={({ field }) => <TextField
                    id="image"
                    label="Image"
                    type="url"
                    fullWidth
                    variant="standard"
                    {...field}/> }/>

                <Controller name={"sound"} control={control} render={({ field }) => <TextField
                    id="sound"
                    label="Sound"
                    type="text"
                    fullWidth
                    variant="standard"
                    {...field}/> }/>


        </DialogContent>
        <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
            <Button type="submit">Submit</Button>
        </DialogActions>
    </form>
    </Dialog>
}

export default AddItemDialog;


// <Input
//     id="name"
//     type="text"
//     placeholder="name"
// />
// <Input
//     id="quote"
//     type="text"
//     placeholder="Quote"
// />
// <Input
//     id="image"
//     type="text"
//     placeholder="Image"
// />
// <Input
//     id="sound"
//     type="text"
//     placeholder="Sound"
// />
