import SoundboardItem from "./SoundboardItem";
import "./Soundboard.css";
import {useEffect, useState} from "react";
import axios from "axios";
import {useParams} from "react-router-dom";
import {Item, ItemData, ItemDataMetSB} from "./model/item";
import {Alert, Fab} from "@mui/material";
import AddIcon from '@mui/icons-material/Add';
import AddItemDialog from "./AddItemDialog";


function Soundboard() {
    const {id} = useParams();
    const [quote, setQuote] = useState<String | null>(null);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [items, setItems] = useState<Item[] | null>(null);
    const [isError, setError] = useState(false);
    const [isLoadingData, setIsLoadingData] = useState(true);
    useEffect(() => {
        async function fetchItems() {
            try {
                const response = await axios.get(`/soundboards/${id}/items`);
                setItems(response.data);
                setIsLoadingData(false);
            }
            catch (error) {
                setError(true);
                setIsLoadingData(false);
            }
        }
        fetchItems();
    }, [id, isLoadingData]);

    if (isError || !items) {
        return <Alert severity="error">Items could not be loaded</Alert>;
    }

    if (isLoadingData) {
        return <>Loading...</>
    }

    async function addItem(item: ItemData) {
        console.log({...item, soundboardId: id!});
        setIsLoadingData(true);
        await axios.post('/items', {...item, soundboardId: id!});
    }


    return (
        <>
            <div className="soundboard">
                {items.map((item) => (
                    <SoundboardItem
                        key={item.id}
                        item={item}
                        onSoundStart={() => setQuote(item.quote)}
                        onSoundEnd={() => setQuote(null)}/>
                ))}
            </div>
            {quote && <div className="soundboardQuote">{quote}</div>}

            <Fab color="primary" aria-label="add" onClick={() => setDialogOpen(!dialogOpen)} sx={{position:"absolute", bottom:"4vw", right:"4vw", height:"100px", width: "100px"}}>
                <AddIcon sx={{height:"60px", width: "60px"}}/>
            </Fab>

            <AddItemDialog open={dialogOpen} closeDialog={() => setDialogOpen(false)} onSubmit={addItem}/>
        </>
    );
}

export default Soundboard;
