import {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import axios from "axios";
import './soundboards.css';

import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import {Board} from "./model/Board";

function Soundboards() {

    const [boards, setBoards] = useState<Board[] | null>(null);
    const [error, setError] = useState<any | null>(null);
    const [isLoadingData, setIsLoadingData] = useState(true);
    const navigate = useNavigate();

    useEffect(() => {
        async function fetchBoards() {
            try {
                const response = await axios.get(`/soundboards`);
                setBoards(response.data);
                setIsLoadingData(false);
            } catch (error: any) {
                setError(error);
                setIsLoadingData(false);
            }
        }

        fetchBoards();
    }, []);

    if (error) {
        return <>An error has occured! ${error.toString()}</>
    }
    if (isLoadingData || boards === null) {
        return <>Loading...</>
    }

    return <div className="soundboards">
        {boards.map((board) => (
            <Card sx={{maxWidth: 600}} onClick={() => navigate(`/soundboards/${board.id}`)} key={board.id}>
                <CardMedia
                    component="img"
                    height="400"
                    image={board.image}
                    alt={board.name}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {board.name}
                    </Typography>
                </CardContent>
            </Card>
        ))}
    </div>


    // return (
    //     <div className="soundboards">
    //         {boards.map((board) => (
    //             <Card sx={{maxWidth: 345}} key={board.id}>
    //                 <CardMedia
    //                     component="img"
    //                     height="140"
    //                     alt="green iguana"
    //                     image={board.image}
    //                 />
    //                 <CardContent>
    //                     <Typography gutterBottom variant="h5" component="div">
    //                         ${board.name}
    //                     </Typography>
    //                 </CardContent>
    //                 <CardActions>
    //                     <Button size="small">Share</Button>
    //                     <Button size="small">Learn More</Button>
    //                 </CardActions>
    //             </Card>
    //             ))}
    //     </div>
    // );
}

export default Soundboards;
