export interface Item {
    id: number;
    name: string;
    image: string;
    sound: string;
    quote: string;
    soundboardId: number;
}

export type ItemData = Omit<Item, "id" | "soundboardId">;

export type ItemDataMetSB = Omit<Item, "id">;
