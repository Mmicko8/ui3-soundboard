import {useState} from "react";
import {Item} from "./model/item";

interface SoundboardItemProps {
    item: Item;
    onSoundStart: () => void;
    onSoundEnd: () => void;
}


function SoundboardItem({item, onSoundStart, onSoundEnd}:SoundboardItemProps) {

    const [isPlaying, setIsPlaying] = useState(false);

    function playSound(soundPath: string) {
        onSoundStart();
        const audio = new Audio("/sounds/" + soundPath);
        audio.addEventListener("canplaythrough", () => {
            audio.play();
        });
        audio.addEventListener("playing", () => {setIsPlaying(true)});
        audio.addEventListener('ended', () => {
            setIsPlaying(false);
            onSoundEnd();
        });
    }

    return (
        <div className={`item ${isPlaying ? "playing" : ""}`} onClick={() => playSound(item.sound)}>
            <img alt={item.name} src={item.image}/>
        </div>
    );
}

export default SoundboardItem;
