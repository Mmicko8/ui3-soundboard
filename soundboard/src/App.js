import './App.css';
import Soundboard from "./Soundboard";
import axios from "axios";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Soundboards from "./SoundBoards";

axios.defaults.baseURL = "http://localhost:3001";

function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/soundboards/:id" element={<Soundboard/>}/>
                <Route path="/" element={<Soundboards/>}/>
                <Route path="/soundboards" element={<Soundboards/>}/>
            </Routes>
        </BrowserRouter>
    )
}

export default App;
